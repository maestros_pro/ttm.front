// Event listenr



$(function(){

	var $b = $('body');

	$b

		.on('submit', '.js-form', function (e) {
			e.preventDefault();
			var $form = $(this);

			if ($.app.checkForm($form)) {

				$.ajax({
					type: $form.attr('method'),
					url: $form.attr('action'),
					data: $form.serialize(),
					complete: function (res) {

						var data = JSON.parse(res.responseText);

						if (res.status) {
							var status = res.status,
								formName = $form.data('form-name');

							if (status == 200) {    /** success */

								switch (formName) {
									case 'feedback':

										break;

									case 'faq':

										break;
								}

							} else if (status == 400) {    /** error */

							} else if (status == 500) {    /** Internal Server Error */

							} else {
								/** other trouble */
								console.error(res);
							}

						}
					}
				});
			}

		})

		.on('click', '.aside__content-link', function(){
			$b.removeClass('_nav-open');
			$.app.fp.start();
		})

		.on('click', '.aside__handle', function(){

			if ( $b.hasClass('_nav-open') ){
				$b.removeClass('_nav-open');
				$.app.fp.start();
			} else {
				$b.addClass('_nav-open');
				$.app.fp.hold();
			}
		})

		.on('click', '.content__menu-link', function(e){

			e.preventDefault();

			var $t = $(this),
				$item = $t.closest('.content__menu-item'),
				$wrap = $t.closest('.content__menu');

			if ( $item.find('.content__menu-list').size() ){

				if ( !$item.hasClass('is-open') ){
					$item.find('.content__menu-list').slideDown(300, function(){ $item.addClass('is-open'); });
					$wrap.find('.is-open').not($item).find('.content__menu-list').slideUp(300, function(){ $(this).closest('.content__menu-item').removeClass('is-open'); });
				}

			} else {
				$item.addClass('is-active');
				$wrap.find('.is-active').not($item).removeClass('is-active');
			}
		})


	;

});