(function(){
	var app = {
		module: {

			checkForm: function(form){
				var $el = $(form),
					wrong = false;

				$el.find('[data-required]').each(function(){
					var $t = $(this),
						type = $t.data('required'),
						$wrap = $t.closest('.form__field'),
						$mes = $wrap.find('.form__field-message'),
						val = $.trim($t.val()),
						errMes = '',
						rexp = /.+$/igm;

					$wrap.removeClass('error success');
					$mes.html('');

					if ( $t.attr('type') == 'checkbox' && !$t.is(':checked') ) {
						val = false;
					} else if ( /^(#|\.)/.test(type) ){
						if ( val !== $(type).val() || !val ) val = false;
					} else if ( /^(name=)/.test(type) ){
						if ( val !== $('['+type+']').val() || !val ) val = false;
					} else if ( $t.attr('type') == 'radio'){
						var name =  $t.attr('name');
						if ( $('input[name='+name+']:checked').length < 1 ) val = false;
					} else {
						switch (type) {
							case 'number':
								rexp = /^\d+$/i;
								errMes = 'Поле должно содержать только числовые символы';
								break;
							case 'phone':
								rexp = /[\+]\d\s[\(]\d{3}[\)]\s\d{3}\s\d{2}\s\d{2}/i;
								break;
							case 'letter':
								rexp = /^[A-zА-яЁё]+$/i;
								break;
							case 'rus':
								rexp = /^[А-яЁё]+$/i;
								break;
							case 'email':
								rexp = /^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/i;
								errMes = 'Проверьте корректность email';
								break;
							case 'password':
								rexp = /^(?=^.{8,}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/;
								errMes = 'Слишком простой пароль';
								break;
							default:
								rexp = /.+$/igm;
						}
					}


					if ( !rexp.test(val) || val == 'false' || !val ){
						wrong = true;
						$wrap.addClass('error');

						if (!val) errMes = 'Поле не заполнено';

						if ( errMes ){

							$mes.html(errMes);
						}

					} else {
						$wrap.addClass('success');

					}

				});


				return !wrong;

			},

			viewPort: {

				current: '',

				data: {
					'0': function(){
						self.viewPort.current = 0;
						console.info('-----mobile-----');
					},
					'640': function(){
						self.viewPort.current = 1;
						console.info('-----tabletVer-----');
					},
					'960': function(){
						self.viewPort.current = 2;
						console.info('-----tabletHor-----');
					},
					'1200': function(){
						self.viewPort.current = 3;
						console.info('-----desktop-----');
					},
					'1500': function(){
						self.viewPort.current = 4;
						console.info('-----desktop HD-----');
					},
					'1800': function(){
						self.viewPort.current = 5;
						console.info('-----full HD-----');
					}
				},

				init: function(data){
					var points = data || self.viewPort.data;
					if ( points ){
						points['Infinity'] = null;
						var sbw = scrollBarWidth(), curPoint = null;
						var ww = $(window).width() + sbw;
						checkCurrentViewport();
						$(window).on('resize', function(){
							ww = $(window).width() + sbw;
							checkCurrentViewport();
						});
					}

					function checkCurrentViewport(){
						var pp = 0, pc = null;
						$.each(points, function(point, callback){
							if ( point > ww ){
								if ( pp !== curPoint ) {
									curPoint = pp;
									pc();
								}
								return false;
							}
							pp = point; pc = callback;
						});
					}

					function scrollBarWidth(){
						var scrollDiv = document.createElement('div');
						scrollDiv.className = 'scroll_bar_measure';
						$(scrollDiv).css({
							width: '100px',
							height: '100px',
							overflow: 'scroll',
							position: 'absolute',
							top: '-9999px'
						});
						document.body.appendChild(scrollDiv);
						sbw = scrollDiv.offsetWidth - scrollDiv.clientWidth;
						document.body.removeChild(scrollDiv);
						return sbw;
					}

				}
			},

			tooltip: {
				init: function(){
					var $tooltip = $('.tooltip');

					if ( !$tooltip.size() ){
						$tooltip = $('<div/>', {'class': 'tooltip'}).appendTo('body');
					}

					$('body').on('mouseover', '[data-tooltip]', function(){
						var $t = $(this);
						var text = $t.attr('data-tooltip');
						$tooltip.html('<span class="tooltip-inner">'+text+'</span>').stop(true, true).delay(1000).addClass('show');

						var top = $t.offset().top + $t.outerHeight();
						var left = $t.offset().left + ($t.outerWidth()/2);
						$tooltip.css({top: top, left: left});

					}).on('mouseleave', '[data-tooltip]', function(){
						$tooltip.removeClass('show');
					});


				}
			},

			popup: {
				create: function(popup){
					var pref = popup.indexOf('#') == 0  ? 'id' : 'class';
					var name = popup.replace(/^[\.#]/,'');
					var $popup = $('<div class="popup">'
						+			'<div class="popup__inner">'
						+				'<div class="popup__layout">'
						+					'<div class="popup__close"></div>'
						+					'<div class="popup__content"></div>'
						+				'</div>'
						+			'</div>'
						+			'<div class="popup__overlay"></div>'
						+		'</div>').appendTo('body');

					if ( pref == 'id'){
						$popup.attr(pref, name);
					} else {
						$popup.addClass(name);
					}

					return $popup;
				},

				open: function(popup, html){
					var $popup = $(popup);
					if (!$popup.size()){
						$popup = self.popup.create(popup);
					}
					if( html ){
						$popup.find('.popup__content').html(html);
					}
					$('body').addClass('overflow_hidden');
					return $popup.show();
				},

				close: function(popup){
					var $popup = $(popup);
					$('body').removeClass('overflow_hidden');
					$popup.hide();
				},

				info: function(mes, callback){
					var html = '<div class="popup__text">' + mes + '</div>'
						+	'<div class="popup__link"><span class="btn btn_green">Ок</span></div>';
					self.popup.open('.popup_info', html);

					$('.popup_info').find('.btn').click(function(){
						self.popup.close($('.popup_info'));
						if ( callback ) callback();
					});
				},

				remove: function(popup){
					var $popup = $(popup);
					$('body').removeClass('overflow_hidden');
					$popup.remove();
				},

				init: function(){
					$('body')
						.on('click', '.popup', function(e){
							if ( !$(e.target).closest('.popup__layout').size() ) self.popup.close('.popup');
						})
						.on('click', '.popup__close, .js-popup-close', function(e){
							e.preventDefault();
							e.stopPropagation();
							self.popup.close($(this).closest('.popup'));
						})
						.on('click', '[data-popup]', function(e){
							e.preventDefault();
							e.stopPropagation();
							self.popup.close('.popup');
							self.popup.open($(this).data('popup'));
						})
					;
				}
			},

			scrollBar: {

				init: function(el){
					$(el).not('.inited').mCustomScrollbar({
						callbacks:{
							onCreate: function(){
								$(this).addClass('inited');
							}
						}
					});
				}
			},

			fp: {

				active: false,

				hold: function(){
					if (self.fp.active){
						$.fn.fullpage.setAllowScrolling(false);
					}
				},

				destroy: function(){
					if (self.fp.active){
						self.fp.active = false;
						$.fn.fullpage.destroy();
					}
				},

				start: function(){
					if (!self.fp.active){
						self.fp.active = true;
						$('#fp').fullpage({
							recordHistory: false
							,anchors: ['welcome', 'main', 'gym', 'masters', 'events', 'sponsors','price','contact']
							,menu: '#fpNav'
							//,scrollOverflow: true
						});
					} else {
						$.fn.fullpage.setAllowScrolling(true);
					}
				}
			},

			tabs: {
				init: function(){
					$('body')
						.on('click', '[data-tab-link]', function(e){
							/**
							 * data-tab-link='name_1', data-tab-group='names'
							 * data-tab-targ='name_1', data-tab-group='names'
							 **/
							e.preventDefault();
							var $t = $(this);
							var group = $t.data('tab-group');
							var $links = $('[data-tab-link]').filter(selectGroup);
							var $tabs = $('[data-tab-targ]').filter(selectGroup);
							var ind = $t.data('tab-link');
							var $tabItem = $('[data-tab-targ='+ind+']').filter(selectGroup);

							if( !$t.hasClass('active')){
								$links.removeClass('active');
								$t.addClass('active');
								$tabs.fadeOut(150);
								setTimeout(function(){
									$tabs.removeClass('active');
									$tabItem.fadeIn(150, function(){
										$(this).addClass('active');
									});
								}, 150)
							}

							function selectGroup(){
								return $(this).data('tab-group') === group;
							}

						})
					;
				}
			},

			slickBottom: {

				timer: null,

				init: function (el) {
					self.slickBottom.calc(el);
					$(window).resize(function () {
						if ( self.slickBottom.timer ) clearTimeout(self.slickBottom.timer);

						self.slickBottom.timer = setTimeout(function () {
							self.slickBottom.init(el);
						}, 200);
					})
				},

				calc: function (el) {

					$(el).each(function () {
						var $el = $(this),
							$wrap = $el.closest('.inner');
						$el.height( $wrap.outerHeight() - parseInt($wrap.css('paddingTop').replace('px', '')) - $el.offset().top );
					});
				}
			},


			loadInside: {

				urlParam: function(name){
					var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
					if (results==null){ return null; }
					else{ return results[1] || 0; }
				},

				load: function(url){

					var $b = $('body'),
						$inside = $('.inside');

					history.pushState(false, false, url);

					if ( url == baseURL ){
						self.fp.start();
						$b.removeClass('show-inside');
					} else {

						$inside.addClass('is-loading');

						$.ajax({
							url: url,
							cache: false
						}).done(function(data){
							self.fp.hold();
							$b.addClass('show-inside');
							var $loadInside = $(data).find('.inside');

							$inside.find('.inside__back').html($loadInside.find('.inside__back').html());
							$inside.find('.inside__content').html($loadInside.find('.inside__content').html());

							self.plugins.update();

							if ( self.loadInside.urlParam('showslide') ){
								$inside.find('.gallery__big-list').slick('slickGoTo', +self.loadInside.urlParam('showslide'));
							}

							$inside.removeClass('is-loading');

						}).fail(function(err){


						});
					}
				},

				init: function(){

					var $b = $('body');

					$b.on('click', '.js-load-inside', function(e){

						var $t = $(this), url = $t.attr('href');


						if ( $('#fp').size() ){
							e.preventDefault();
							self.loadInside.load(url);
						}


					});


					//setTimeout(function() {
					//	window.onpopstate = function() {
					//		location.reload();
					//	}
					//}, 0);

				}
			},

			welcomeCarousel: {
				init: function () {
					var $carousel = $('.welcome__carousel');

					$carousel.slick({
						slidesToShow: 1,
						slidesToScroll: 1,
						swipeToSlide: true,
						arrows: false,
						autoplay: true,
						autoplaySpeed: 2000,
						pauseOnHover: false
					});

					setTimeout(function () {
						$(window).trigger('resize');
					}, 10);
				}
			},

			plugins: {
				update: function(){

					self.scrollBar.init('.js-scroll-wrap');
					self.slickBottom.init('.js-slick-bottom');

					$('.coaches__list .masters__list').each(function(){
						var $t = $(this);

						if ( !$t.hasClass('is-inited')){
							$t.addClass('is-inited')
								.on('init', function (slick) {

								})
								.on('beforeChange', function (event, slick, currentSlide, nextSlide) {

								})
								.slick({
								slidesToShow: 6,
								slidesToScroll: 1,
								swipe: false,
								vertical: true,
								prevArrow: $('.coaches__control-arrow_prev'),
								nextArrow: $('.coaches__control-arrow_next')
							});
						}
					});

					$('.coaches__card-gallery-list').each(function(){
						var $t = $(this);

						if ( !$t.hasClass('is-inited')){
							$t.addClass('is-inited')
								.on('init', function (slick) {

								})
								.on('beforeChange', function (event, slick, currentSlide, nextSlide) {

								})
								.slick({
								slidesToShow: 3,
								slidesToScroll: 1,
								swipeToSlide: true,
								prevArrow: $('.coaches__card-gallery-arrow_prev'),
								nextArrow: $('.coaches__card-gallery-arrow_next')
							});
						}
					});


					$('.gallery').each(function () {

						var $t = $(this),
							$wrap = $t.closest('.inside'),
							$galleryBig = $wrap.find('.gallery').size() > 1 ? $t.find('.gallery__big-list') : $wrap.find('.gallery__big-list'),
							$galleryPreview = $wrap.find('.gallery').size() > 1 ? $t.find('.gallery__preview') : $wrap.find('.gallery__preview'),
							$galleryScroll = $wrap.find('.gallery__scroll'),
							$counter = $wrap.find('.js-gallery-counter'),

							optionsBig = {
								slidesToShow: 1,
								slidesToScroll: 1,
								swipeToSlide: true,
								prevArrow: $t.find('.gallery__big-arrow_prev'),
								nextArrow: $t.find('.gallery__big-arrow_next'),
								asNavFor: $galleryPreview
							},

							optionsPreview = {
								slidesToShow: 3,
								slidesToScroll: 1,
								swipeToSlide: true,
								arrows: false,
								asNavFor: $galleryBig,
								focusOnSelect: true
							};


						if (!$galleryBig.hasClass('is-inited')){
							$galleryBig
								.addClass('is-inited')
								.on('init', function (slick) {})
								.on('beforeChange', function (event, slick, currentSlide, nextSlide) {

									if ( $galleryScroll.size() ){
										$galleryScroll
											.find('.gallery__scroll-item')
											.removeClass('is-active')
											.eq(nextSlide)
											.addClass('is-active');

									}

									if ( $counter.size() ) $counter.html( (nextSlide + 1) + '/' + slick.slideCount );

								})
								.slick(optionsBig);
						}

						if (!$galleryPreview.hasClass('is-inited')) {

							$galleryPreview
								.addClass('is-inited')
								.on('init', function (slick) {})
								.on('beforeChange', function (event, slick, currentSlide, nextSlide) {})
								.slick(optionsPreview);
						}

						$galleryScroll.on('click', '.gallery__scroll-item', function(){
							var $t = $(this);

							if (!$t.hasClass('is-active') && !$t.hasClass('js-load-inside') && $galleryBig.size()){
								$galleryBig.slick('slickGoTo', $t.index());
							}
						});

					})
				}
			}

		},

		init: function() {

			//self.viewPort.init();
			//self.preLoad.init();
			//self.tooltip.init();
			//self.scrollBar.init('.js-scroll-wrap');
			self.popup.init();
			self.welcomeCarousel.init();

			//$('[data-required=phone]').mask('+7 (999) 999-99-99');

			self.loadInside.init();
			self.fp.start();
			self.plugins.update();

			var $b = $('body');

			$b
				.on('click', '[data-scrollto]', function(e){
					e.preventDefault();
					var $el = $(this).data('scrollto');
					$('html,body').animate({scrollTop: $($el).offset().top}, 500);
				})

			;
		}
	};
	var self = {};
	var loader = function(){
		self = app.module;
		jQuery.app = app.module;
		app.init();
	};
	var ali = setInterval(function(){
		if (typeof jQuery !== 'function') return;
		clearInterval(ali);
		setTimeout(loader, 0);
	}, 50);

})();



